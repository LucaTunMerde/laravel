<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellerSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_sheets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("unicorn_id")->unsigned();
            $table->foreign("unicorn_id")->references("id")->on("unicorns");
            $table->bigInteger("seller_id")->unsigned();
            $table->foreign("seller_id")->references("id")->on("users");
            $table->string("reference");
            $table->float("price");
            $table->string("sell_place");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_sheets');
    }
}

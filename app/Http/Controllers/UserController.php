<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Role;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $users = User::all();

        return view("users.index", compact('users'));
    }

    public function create(){
        $roles = Role::all();
        return view("users.create",compact('roles'));
    }

    public function store(Request $request){
        $user = new User();
        $user->name = $request->get('name') != null ? $request->get('name') : 'undefined';
        $user->role_id = $request->get('role_id');
        $user->email = $request->get('email') != null ? $request->get('email') : 'undefined';
        $user->password = "";
        $user->save();

        $roles = Role::all();
        return redirect()->route('users.index',compact('roles'));
    }

    public function show($id){
        $user = User::find($id);

        return view('users.show',compact('user'));
    }

    public function edit($id){
        $user = User::find($id);
        $roles = Role::all();
        return view('users.edit',compact('user','roles'));
    }

    public function update(Request $request,$id){
        $user = User::find($id);
        $user->name = $request->get('name') != null ? $request->get('name') : $user->name;
        $user->email = $request->get('email') != null ? $request->get('email') : $user->mail;
        $user->role_id = $request->get('role_id') != null ? $request->get('role_id') : $user->role_id;
        $user->save();

        $roles = Role::all();
        return view('users.edit',compact('user','roles'));
    }

    public function destroy(Request $request){
        $user = User::find($request->get('id'));
        $user->delete();

        return redirect()->route('users.index');
    }
}

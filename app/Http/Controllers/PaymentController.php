<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Config;
use Stripe;

use App\Models\Seller_sheet;

class PaymentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $public_key = config('stripe.public_key');
        return view('payments.index',compact('public_key'));
    }


    public function buy($id){
        $seller_sheet = Seller_sheet::find($id);


        if(PaymentController::isBuyable($seller_sheet)){
            return view('payments.buy', compact('seller_sheet'));
            //PaymentController::pay($seller_sheet);
        }else{
            return view('payments.error');
        }


    }

    public function isBuyable($seller_sheet){
        if(is_null($seller_sheet->seller_id) || is_null($seller_sheet->seller) ||
        is_null($seller_sheet->seller->name) || is_null($seller_sheet->seller->email) ||
        is_null($seller_sheet->unicorn_id) || is_null($seller_sheet->unicorn) ||
        is_null($seller_sheet->unicorn->name) || is_null($seller_sheet->reference) ||
        is_null($seller_sheet->price) || $seller_sheet->price <=0 || $seller_sheet->bought
        ){
            return false;
        }

        return true;
    }

    public function paymentProcess(Request $request){
        Stripe\Stripe::setApiKey(config('stripe.private_key'));

        $seller_sheet = Seller_sheet::find($request->id);


        $token = $request->stripeToken;
        $charge = Stripe\Charge::create([
            'amount' => $seller_sheet->price * 100,
            'currency' => 'eur',
            'description' => 'Unicorn payment, ref: '. $seller_sheet->reference . ' to ' . $seller_sheet->seller->name,
            'source' => $token,
        ]);

        $seller_sheet->bought = 1;
        $seller_sheet->save();

        return view('payments.success');
    }
}

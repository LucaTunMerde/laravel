<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Unicorn;

class UnicornController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $unicorns = Unicorn::all();

        return view("unicorns.index", compact('unicorns'));
    }

    public function create(){

        return view("unicorns.create");
    }

    public function store(Request $request){
        $unicorn = new Unicorn();
        $unicorn->name = $request->get('name') != null ? $request->get('name') : 'undefined';
        $unicorn->tourniquet = $request->get('tourniquet') != null ? $request->get('tourniquet') : '0';
        $unicorn->birth_date = $request->get('birth_date') != null ? $request->get('birth_date'): '0000-01-01';
        $unicorn->birth_place = $request->get('birth_place') != null ? $request->get('birth_place') : 'undefined';
        $unicorn->mane_color = $request->get('mane_color') != null ? $request->get('mane_color') : 'white';
        $unicorn->sex = $request->get('sex')  != null ? $request->get('sex'): 0;
        $unicorn->save();

        return redirect()->route('unicorns.index');
    }

    public function show($id){
        $unicorn = Unicorn::find($id);

        return view('unicorns.show',compact('unicorn'));
    }

    public function edit($id){
        $unicorn = Unicorn::find($id);

        return view('unicorns.edit',compact('unicorn'));
    }

    public function update(Request $request,$id){
        $unicorn = Unicorn::find($id);
        $unicorn->name = $request->get('name') != null ? $request->get('name') : $unicorn->name;
        $unicorn->tourniquet = $request->get('tourniquet') != null ? $request->get('tourniquet') : $unicorn->tourniquet;
        $unicorn->birth_date = $request->get('birth_date') != null ? $request->get('birth_date') : $unicorn->birth_date;
        $unicorn->birth_place = $request->get('birth_place') != null ? $request->get('birth_place') : $unicorn->birth_place;
        $unicorn->mane_color = $request->get('mane_color') != null ? $request->get('mane_color') : $unicorn->mane_color;
        $unicorn->sex = $request->get('sex') != null ? $request->get('sex') : $unicorn->sex;
        $unicorn->save();

        return view('unicorns.edit',compact('unicorn'));
    }

    public function destroy(Request $request){
        $unicorn = Unicorn::find($request->get('id'));
        $unicorn->delete();

        return redirect()->route('unicorns.index');
    }

}

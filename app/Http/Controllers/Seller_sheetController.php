<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Seller_sheet;
use App\Models\Unicorn;
use App\Models\Role;
use App\User;
class Seller_sheetController extends Controller
{
    public function index(){
        $seller_sheets = Seller_sheet::all();

        return view("seller_sheets.index", compact('seller_sheets'));
    }

    public function create(){
        $role = Role::where('name','seller')->get()->last();

        if(!is_null($role)){
            $sellers = User::where('role_id',$role->id)->get();
        }else{
            $sellers = User::all();
        }

        $unicorns = Unicorn::all();
        return view("seller_sheets.create",compact('sellers','unicorns'));
    }

    public function store(Request $request){
        $seller_sheet = new Seller_sheet();
        $seller_sheet->unicorn_id = $request->get('unicorn_id') != null ? $request->get('unicorn_id') : 0;
        $seller_sheet->seller_id = $request->get('seller_id') != null ? $request->get('seller_id') : 0;
        $seller_sheet->reference = $request->get('reference') != null ? $request->get('reference') : '--';
        $seller_sheet->price = $request->get('price') != null ? $request->get('price') : '0';
        $seller_sheet->sell_place = $request->get('sell_place') != null ? $request->get('sell_place') : 'undefined';
        $seller_sheet->save();

        return redirect()->route('seller_sheets.index');
    }

    public function show($id){
        $seller_sheet = Seller_sheet::find($id);

        return view('seller_sheets.show',compact('seller_sheet'));
    }

    public function edit($id){
        $seller_sheet = Seller_sheet::find($id);
        $role = Role::where('name','seller')->get()->last();

        if(!is_null($role)){
            $sellers = User::where('role_id',$role->id)->get();
        }else{
            $sellers = User::all();
        }
        $unicorns = Unicorn::all();

        return view('seller_sheets.edit',compact('seller_sheet','sellers','unicorns'));
    }

    public function update(Request $request,$id){
        $seller_sheet = Seller_sheet::find($id);
        $seller_sheet->unicorn_id = $request->get('unicorn_id') != null ? $request->get('unicorn_id') : $seller_sheet->unicorn_id;
        $seller_sheet->seller_id = $request->get('seller_id') != null ? $request->get('seller_id') : $seller_sheet->seller_id;
        $seller_sheet->reference = $request->get('reference') != null ? $request->get('reference') : $seller_sheet->reference;
        $seller_sheet->price = $request->get('price') != null ? $request->get('price') : $seller_sheet->price;
        $seller_sheet->sell_place = $request->get('sell_place') != null ? $request->get('sell_place') : $seller_sheet->sell_place;
        $seller_sheet->save();
        $role = Role::where('name','seller')->get()->last();

        if(!is_null($role)){
            $sellers = User::where('role_id',$role->id)->get();
        }else{
            $sellers = User::all();
        }
        $unicorns = Unicorn::all();

        return view('seller_sheets.edit',compact('seller_sheet','sellers','unicorns'));
    }

    public function destroy(Request $request){
        $seller_sheet = Seller_sheet::find($request->get('id'));
        $seller_sheet->delete();

        return redirect()->route('seller_sheets.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Role;

class RoleController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $roles = Role::all();

        return view("roles.index", compact('roles'));
    }

    public function create(){

        return view("roles.create");
    }

    public function store(Request $request){
        $role = new Role();
        $role->name = $request->get('name') != null ? $request->get('name') : 'undefined';
        $role->save();

        return redirect()->route('roles.index');
    }

    public function show($id){
        $role = Role::find($id);

        return view('roles.show',compact('role'));
    }

    public function edit($id){
        $role = Role::find($id);

        return view('roles.edit',compact('role'));
    }

    public function update(Request $request,$id){
        $role = Role::find($id);
        $role->name = $request->get('name') != null ? $request->get('name') : $role->name;
        $role->save();

        return view('roles.edit',compact('role'));
    }

    public function destroy(Request $request){
        $role = Role::find($request->get('id'));
        $role->delete();

        return redirect()->route('roles.index');
    }
}

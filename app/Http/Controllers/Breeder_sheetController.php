<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Breeder_sheet;
use App\Models\Unicorn;
use App\Models\Role;
use App\User;

class Breeder_sheetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $breeder_sheets = Breeder_sheet::all();

        return view("breeder_sheets.index", compact('breeder_sheets'));
    }

    public function create(){
        $role = Role::where('name','breeder')->get()->last();

        if(!is_null($role)){
            $breeders = User::where('role_id',$role->id)->get();
        }else{
            $breeders = User::all();
        }
        $unicorns = Unicorn::all();
        return view("breeder_sheets.create",compact('breeders','unicorns'));
    }

    public function store(Request $request){
        $breeder_sheet = new Breeder_sheet();
        $breeder_sheet->reference = $request->get('reference') != null ? $request->get('reference') : '--';
        $breeder_sheet->unicorn_id = $request->get('unicorn_id') != null ? $request->get('unicorn_id') : 0;
        $breeder_sheet->breeder_id = $request->get('breeder_id') != null ? $request->get('breeder_id') : 0;
        $breeder_sheet->reproduction_date1 = $request->get('reproduction_date1') != null ? $request->get('reproduction_date1') : '0000-01-01';
        $breeder_sheet->reproduction_date2 = $request->get('reproduction_date2') != null ? $request->get('reproduction_date2') : '0000-01-01';
        $breeder_sheet->is_pregnant = ($request->get('is_pregnant') != null ? ($request->get('is_pregnant') == "on"? 1 : 0) : 0);
        $breeder_sheet->nb_childs = $request->get('nb_childs') != null ? $request->get('nb_childs') : 0;

        $breeder_sheet->save();

        return redirect()->route('breeder_sheets.index');
    }

    public function show($id){
        $breeder_sheet = Breeder_sheet::find($id);

        return view('breeder_sheets.show',compact('breeder_sheet'));
    }

    public function edit($id){
        $breeder_sheet = Breeder_sheet::find($id);
        $role = Role::where('name','breeder')->get()->last();

        if(!is_null($role)){
            $breeders = User::where('role_id',$role->id)->get();
        }else{
            $breeders = User::all();
        }
        $unicorns = Unicorn::all();

        return view('breeder_sheets.edit',compact('breeder_sheet','breeders','unicorns'));
    }

    public function update(Request $request,$id){
        $breeder_sheet = Breeder_sheet::find($id);
        $breeder_sheet->reference = $request->get('reference') != null ? $request->get('reference') : $breeder_sheet->reference;
        $breeder_sheet->unicorn_id = $request->get('unicorn_id') != null ? $request->get('unicorn_id') : $breeder_sheet->unicorn_id;
        $breeder_sheet->breeder_id = $request->get('breeder_id') != null ? $request->get('breeder_id') : $breeder_sheet->breeder_id;
        $breeder_sheet->reproduction_date1 = $request->get('reproduction_date1') != null ? $request->get('reproduction_date1') : $breeder_sheet->reproduction_date1;
        $breeder_sheet->reproduction_date2 = $request->get('reproduction_date2') != null ? $request->get('reproduction_date2') : $breeder_sheet->reproduction_date2;
        $breeder_sheet->is_pregnant = ($request->get('is_pregnant') != null ? ($request->get('is_pregnant') == "on"? 1 : 0) : $breeder_sheet->is_pregnant);
        $breeder_sheet->nb_childs = $request->get('nb_childs') != null ? $request->get('nb_childs') : $breeder_sheet->nb_childs;
        $breeder_sheet->save();

        $role = Role::where('name','breeder')->get()->last();

        if(!is_null($role)){
            $breeders = User::where('role_id',$role->id)->get();
        }else{
            $breeders = User::all();
        }
        $unicorns = Unicorn::all();

        return view('breeder_sheets.edit',compact('breeder_sheet','breeders','unicorns'));
    }

    public function destroy(Request $request){
        $breeder_sheet = Breeder_sheet::find($request->get('id'));
        $breeder_sheet->delete();

        return redirect()->route('breeder_sheets.index');
    }
}

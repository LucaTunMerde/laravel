<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;

use App\User;
use App\Models\Unicorn;
use App\Models\Seller_sheet;
use App\Models\Breeder_sheet;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'nb_users' => count(User::all()),
            'nb_unicorns' => count(Unicorn::all()),
            'nb_seller_sheets' => count(Seller_sheet::all()),
            'nb_breeder_sheets' => count(Breeder_sheet::all()),
        ];

        return view('home',compact('data'));
    }


    public function postChangeLanguage(Request $request)
	{

		$supported = [
            'en','fr' //list of supported languages of your application.
		];

		$language = $request->get('lang'); //lang is name of form select field.


        if (array_key_exists($language, config('languages'))) {
            session(['applocale'=> $language]);
        }
        //dd(session('applocale'));
        return redirect()->back();
	}

    public function search(Request $request){

        $terms = explode(' ',$request->get('search'));
        $results = array();

        foreach ($terms as $term) {

            $users = User::where(
                'name', 'like', '%'.$term.'%'
            )->orWhere(
                'email', 'like', '%'.$term.'%'
            )->orderby(
                'name'
            )->get();

            $results['users'] = $users;

            $unicorns = Unicorn::where(
                'name', 'like', '%'.$term.'%'
            )->orWhere(
                'tourniquet', 'like', '%'.$term.'%'
            )->orWhere(
                'birth_date', '=', $term
            )->orWhere(
                'birth_place', 'like', '%'.$term.'%'
            )->orWhere(
                'mane_color', 'like', '%'.$term.'%'
            )->orderby(
                'name'
            )->get();

            $results['unicorns'] = $unicorns;

            $seller_sheets = Seller_sheet::where(
                'reference', 'like', '%'.$term.'%'
            )->orWhere(
                'sell_place', 'like', '%'.$term.'%'
            )->orderby(
                'reference'
            )->get();

            $results['seller_sheets'] = $seller_sheets;

            $breeder_sheets = Breeder_sheet::where(
                'reference', 'like', '%'.$term.'%'
            )->orWhere(
                'reproduction_date1', '=', $term
            )->orWhere(
                'reproduction_date2', '=', $term
            )->get();

            $results['breeder_sheets'] = $breeder_sheets;



        }
        //dd($results);
        return view('search', compact('results'));
    }

}

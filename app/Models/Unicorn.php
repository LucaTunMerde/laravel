<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unicorn extends Model
{
    protected $table = "unicorns";

    protected $fillable = [
            "id", "name", "tourniquet", "birth_date", "birth_place",
            "mane_color", "sex"
    ];

    public function seller_sheets(){
        return $this->hasMany(Seller_sheets::class);
    }
}

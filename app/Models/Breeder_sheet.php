<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Unicorn;

class Breeder_sheet extends Model
{
    protected $table = "breeder_sheets";

    protected $fillable = [
            "id", "reference", "reproduction_date1", "reproduction_date2",
            "is_pregnant", "nb_childs"
    ];

    public function unicorn(){
        return $this->belongsTo(Unicorn::class, 'unicorn_id');
    }

    public function breeder(){
        return $this->belongsTo(User::class, 'breeder_id');
    }
}

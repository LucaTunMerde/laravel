<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Seller_sheet extends Model
{
    protected $table = "seller_sheets";

    protected $fillable = [
            "id", "reference", "price", "sell_place"
    ];

    public function unicorn(){
        return $this->belongsTo(Unicorn::class, 'unicorn_id');
    }

    public function seller(){
        return $this->belongsTo(User::class, 'seller_id');
    }


}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('breeder_sheets.breeder_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('breeder_sheets.create') }}" class="btn btn-secondary">{{__('forms.add').__('breeder_sheets.breeder_sheet')}}</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{__('breeder_sheets.reference')}}</th>
                                <th scope="col">{{__('breeder_sheets.breeder')}}</th>
                                <th scope="col">{{__('breeder_sheets.unicorn')}}</th>
                                <th scope="col">{{__('breeder_sheets.reproduction_date1')}}</th>
                                <th scope="col">{{__('breeder_sheets.reproduction_date2')}}</th>
                                <th scope="col">{{__('breeder_sheets.pregnant')}}</th>
                                <th scope="col">{{__('breeder_sheets.nb_childs')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($breeder_sheets as $breeder_sheet)
                            <tr>
                                @if(!is_null($breeder_sheet->id) && !is_null($breeder_sheet->reference))
                                <td><a href="{{route("breeder_sheets.show",$breeder_sheet->id)}}">{{$breeder_sheet->reference}}</a></td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($breeder_sheet->breeder_id) && !is_null($breeder_sheet->breeder) && !is_null($breeder_sheet->breeder->name))
                                <td>{{$breeder_sheet->breeder->name}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($breeder_sheet->unicorn_id) && !is_null($breeder_sheet->unicorn) && !is_null($breeder_sheet->unicorn->name))
                                <td>{{$breeder_sheet->unicorn->name}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($breeder_sheet->reproduction_date1))
                                <td>{{$breeder_sheet->reproduction_date1}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($breeder_sheet->reproduction_date2))
                                <td>{{$breeder_sheet->reproduction_date2}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($breeder_sheet->is_pregnant))
                                <td>{{$breeder_sheet->is_pregnant?__('global.yes'):__('global.no')}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($breeder_sheet->nb_childs))
                                <td>{{$breeder_sheet->nb_childs}}</td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('breeder_sheets.breeder_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('breeder_sheets.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('breeder_sheets.update',$breeder_sheet->id)}}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="reference">{{__('breeder_sheets.reference')}}</label>
                            @if(!is_null($breeder_sheet->reference))
                            <input type="text" name="reference" id="reference" value="{{$breeder_sheet->reference}}" class="form-control">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="breeder_id">{{__('breeder_sheets.breeder')}}</label>
                            <select name="breeder_id" id="breeder_id" class="form-control">
                                <option value="0"></option>
                                @foreach($breeders as $breeder)
                                    <option value="{{$breeder->id}}" {{$breeder_sheet->breeder_id == $breeder->id ? 'selected':''}}>{{$breeder->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="unicorn_id">{{__('breeder_sheets.unicorn')}}</label>
                            <select name="unicorn_id" id="unicorn_id" class="form-control">
                                <option value="0"></option>
                                @foreach($unicorns as $unicorn)
                                    <option value="{{$unicorn->id}}" {{$breeder_sheet->unicorn_id == $unicorn->id ? 'selected':''}}>{{$unicorn->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="reproduction_date1">{{__('breeder_sheets.reproduction_date1')}}</label>
                            @if(!is_null($breeder_sheet->reproduction_date1))
                            <input type="date" name="reproduction_date1" id="reproduction_date1" value="{{$breeder_sheet->reproduction_date1}}" class="form-control">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="reproduction_date2">{{__('breeder_sheets.reproduction_date2')}}</label>
                            @if(!is_null($breeder_sheet->reproduction_date2))
                            <input type="date" name="reproduction_date2" id="reproduction_date2" value="{{$breeder_sheet->reproduction_date2}}" class="form-control">
                            @endif
                        </div>


                        <div class="form-check form-check-inline">
                            <input type="radio" name="is_pregnant" id="is_pregnant" class="form-check-input" {{$breeder_sheet->isPregant==1?'checked':''}}>
                            <label for="is_pregnant" class="form-check-label">{{__('breeder_sheets.pregnant')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" name="is_pregnant" id="is_not_pregnant" class="form-check-input" {{$breeder_sheet->isPregant==0?'checked':''}}>
                            <label for="is_not_pregnant" class="form-check-label">{{__('breeder_sheets.notPregnant')}}</label>
                        </div>

                        <div class="form-group">
                            <label for="nb_childs">{{__('breeder_sheets.nb_childs')}}</label>
                            @if(!is_null($breeder_sheet->nb_childs))
                            <input type="text" name="nb_childs" id="nb_childs" value="{{$breeder_sheet->nb_childs}}" class="form-control">
                            @endif
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('breeder_sheets.breeder_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('breeder_sheets.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('breeder_sheets.store')}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="reference">{{__('breeder_sheets.reference')}}</label>
                            <input type="text" name="reference" id="reference" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="breeder_id">{{__('breeder_sheets.breeder')}}</label>
                            <select name="breeder_id" id="breeder_id" class="form-control">
                                <option value="0"></option>
                                @foreach($breeders as $breeder)
                                    <option value="{{$breeder->id}}">{{$breeder->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="unicorn_id">{{__('breeder_sheets.unicorn')}}</label>
                            <select name="unicorn_id" id="unicorn_id" class="form-control">
                                <option value="0"></option>
                                @foreach($unicorns as $unicorn)
                                    <option value="{{$unicorn->id}}">{{$unicorn->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="reproduction_date1">{{__('breeder_sheets.reproduction_date1')}}</label>
                            <input type="date" name="reproduction_date1" id="reproduction_date1" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="reproduction_date2">{{__('breeder_sheets.reproduction_date2')}}</label>
                            <input type="date" name="reproduction_date2" id="reproduction_date2" class="form-control">
                        </div>

                        <div class="form-check form-check-inline">
                            <input type="radio" name="is_pregnant" id="is_pregnant" class="form-check-input">
                            <label for="is_pregnant" class="form-check-label">{{__('breeder_sheets.pregnant')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" name="is_pregnant" id="is_not_pregnant" class="form-check-input" checked>
                            <label for="is_not_pregnant" class="form-check-label">{{__('breeder_sheets.notPregnant')}}</label>
                        </div>

                        <div class="form-group">
                            <label for="nb_childs">{{__('breeder_sheets.nb_childs')}}</label>
                            <input type="text" name="nb_childs" id="nb_childs" class="form-control">
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('breeder_sheets.breeder_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('breeder_sheets.index') }}" class="btn btn-secondary">{{__('forms.return_to_list')}}</a>
                    <a href="{{ route('breeder_sheets.edit',$breeder_sheet->id) }}" class="btn btn-info">{{__('forms.edit').__('breeder_sheets.breeder_sheet')}}</a>
                    <form action="{{ route('breeder_sheets.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $breeder_sheet->id }}">
                        <button class="btn btn-danger" type="submit">{{__('forms.delete').__('breeder_sheets.breeder_sheet')}}</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="reference" class="col-sm-2 col-form-label">{{__('breeder_sheets.reference')}}</label>
                            @if(!is_null($breeder_sheet->reference))
                            <div class="col-sm-10">
                                <input id="reference" readonly type="text" name="reference" class="form-control-plaintext" value="{{$breeder_sheet->reference}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="breeder" class="col-sm-2 col-form-label">{{__('breeder_sheets.breeder')}}</label>
                            @if(!is_null($breeder_sheet->breeder_id) && !is_null($breeder_sheet->breeder) && !is_null($breeder_sheet->breeder->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="breeder" id="breeder" class="form-control-plaintext" value="{{$breeder_sheet->breeder->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="unicorn" class="col-sm-2 col-form-label">{{__('breeder_sheets.unicorn')}}</label>
                            @if(!is_null($breeder_sheet->unicorn_id) && !is_null($breeder_sheet->breeder) && !is_null($breeder_sheet->breeder->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="unicorn" id="unicorn" class="form-control-plaintext" value="{{$breeder_sheet->unicorn->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="reproduction_date1" class="col-sm-2 col-form-label">{{__('breeder_sheets.reproduction_date1')}}</label>
                            @if(!is_null($breeder_sheet->reproduction_date1))
                            <div class="col-sm-10">
                                <input type="text" readonly name="reproduction_date1" id="reproduction_date1" class="form-control-plaintext" value="{{$breeder_sheet->reproduction_date1}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="reproduction_date2" class="col-sm-2 col-form-label">{{__('breeder_sheets.reproduction_date2')}}</label>
                            @if(!is_null($breeder_sheet->reproduction_date2))
                            <div class="col-sm-10">
                                <input type="text" readonly name="reproduction_date2" id="reproduction_date2" class="form-control-plaintext" value="{{$breeder_sheet->reproduction_date2}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="is_pregnant" class="col-sm-2 col-form-label">{{__('breeder_sheets.pregnant')}}</label>
                            @if(!is_null($breeder_sheet->is_pregnant))
                            <div class="col-sm-10">
                                <input type="text" readonly name="is_pregnant" id="is_pregnant" class="form-control-plaintext" value="{{$breeder_sheet->is_pregnant?__('breeder_sheets.pregnant'):__('breeder_sheets.notPregnant')}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="nb_childs" class="col-sm-2 col-form-label">{{__('breeder_sheets.nb_childs')}}</label>
                            @if(!is_null($breeder_sheet->nb_childs))
                            <div class="col-sm-10">
                                <input type="text" readonly name="nb_childs" id="nb_childs" class="form-control-plaintext" value="{{$breeder_sheet->nb_childs}}">
                            </div>
                            @endif
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('users.users')}}</div>

                <div class="card-body">
                    <a href="{{ route('users.index') }}" class="btn btn-secondary">{{__('forms.return_to_list')}}</a>
                    <a href="{{ route('users.edit',$user->id) }}" class="btn btn-info">{{__('forms.edit').__('users.user')}}</a>
                    <form action="{{ route('users.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <button class="btn btn-danger" type="submit">{{__('forms.delete').__('users.user')}}</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">{{__('users.name')}}</label>
                            @if(!is_null($user->name))
                            <div class="col-sm-10">
                                <input id="name" readonly type="text" name="name" class="form-control-plaintext" value="{{$user->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">{{__('users.email')}}</label>
                            @if(!is_null($user->email))
                            <div class="col-sm-10">
                                <input type="text" readonly name="email" id="email" class="form-control-plaintext" value="{{$user->email}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="role" class="col-sm-2 col-form-label">{{__('users.role')}}</label>
                            @if(!is_null($user->role_id) && !is_null($user->role) && !is_null($user->role->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="role" id="role" class="form-control-plaintext" value="{{$user->role->name}}">
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

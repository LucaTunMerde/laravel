@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('users.user')}}</div>

                <div class="card-body">
                    <a href="{{ route('users.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('users.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{__('users.name')}}</label>
                            <input id="name" type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="email">{{__('users.email')}}</label>
                            <input type="text" name="email" id="email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="role_id">{{__('users.role')}}</label>
                            <select class="form-control" name="role_id">
                                <option value="0"></option>
                                @foreach($roles as $role){
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                }
                                @endforeach
                            </select>
                        </div>



                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

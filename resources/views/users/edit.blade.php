@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('users.user')}}</div>

                <div class="card-body">
                    <a href="{{ route('users.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('users.update',$user->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">{{__('users.name')}}</label>
                            @if(!is_null($user->name))
                                <input id="name" type="text" name="name" class="form-control" value="{{$user->name}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email">{{__('users.email')}}</label>
                            @if(!is_null($user->email))
                                <input id="email" type="text" name="email" class="form-control" value="{{$user->email}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="role_id">{{__('users.role')}}</label>
                            <select class="form-control" name="role_id">
                                @if(is_null($user->role_id))
                                <option value="0" selected></option>
                                @else
                                <option value="0"></option>
                                @endif
                                @foreach($roles as $role){
                                    <option value="{{$role->id}}" {{$user->role_id == $role->id?'selected':''}}>{{$role->name}}</option>
                                }
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

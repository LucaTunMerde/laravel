@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('users.users')}}</div>

                <div class="card-body">
                    <a href="{{ route('users.create') }}" class="btn btn-secondary">{{__('forms.add').__('users.user')}}</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{__('users.name')}}</th>
                                <th scope="col">{{__('users.email')}}</th>
                                <th scope="col">{{__('users.role')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                @if(!is_null($user->id) && !is_null($user->name))
                                <td><a href="{{route("users.show",$user->id)}}">{{$user->name}}</a></td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($user->email))
                                <td>{{$user->email}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($user->role_id) && !is_null($user->role) && !is_null($user->role->name))
                                <td>{{$user->role->name}}</td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

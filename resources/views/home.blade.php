@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <p>{{__('global.number of').__('users.users')}}: {{$data['nb_users']}}</p>
                    <p>{{__('global.number of').__('unicorns.unicorns')}}: {{$data['nb_unicorns']}}</p>
                    <p>{{__('global.number of').__('seller_sheets.seller_sheets')}}: {{$data['nb_seller_sheets']}}</p>
                    <p>{{__('global.number of').__('breeder_sheets.breeder_sheets')}}: {{$data['nb_breeder_sheets']}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

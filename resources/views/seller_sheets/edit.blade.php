@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('seller_sheets.seller_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('seller_sheets.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('seller_sheets.update',$seller_sheet->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="reference">{{__('seller_sheets.reference')}}</label>
                            @if(!is_null($seller_sheet->reference))
                                <input type="text" name="reference" id="reference" class="form-control" value="{{$seller_sheet->reference}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="seller_id">{{__('seller_sheets.seller')}}</label>
                            <select name="seller_id" id="seller_id" class="form-control">
                                <option value="0"></option>
                                @foreach($sellers as $seller)
                                    <option value="{{$seller->id}}" {{$seller_sheet->seller_id == $seller->id ? 'selected':''}}>{{$seller->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="unicorn_id">{{__('seller_sheets.unicorn')}}</label>
                            <select name="unicorn_id" id="unicorn_id" class="form-control">
                                <option value="0"></option>
                                @foreach($unicorns as $unicorn)
                                    <option value="{{$unicorn->id}}" {{$seller_sheet->unicorn_id == $unicorn->id ? 'selected':''}}>{{$unicorn->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="price">{{__('seller_sheets.price')}}</label>
                            @if(!is_null($seller_sheet->price))
                            <input type="text" name="price" id="price" class="form-control" value="{{$seller_sheet->price}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="sell_place">{{__('seller_sheets.sell_place')}}</label>
                            @if(!is_null($seller_sheet->sell_place))
                            <input type="text" name="sell_place" class="form-control" id="sell_place" value="{{$seller_sheet->sell_place}}">
                            @endif
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

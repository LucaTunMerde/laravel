@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('seller_sheets.seller_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('seller_sheets.create') }}" class="btn btn-secondary">{{__('forms.add').__('seller_sheets.seller_sheet')}}</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{__('seller_sheets.reference')}}</th>
                                <th scope="col">{{__('seller_sheets.seller')}}</th>
                                <th scope="col">{{__('seller_sheets.unicorn')}}</th>
                                <th scope="col">{{__('seller_sheets.price')}}(€)</th>
                                <th scope="col">{{__('seller_sheets.sell_place')}}</th>
                                <th scope="col">{{__('payments.bought')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($seller_sheets as $seller_sheet)
                            <tr>
                                @if(!is_null($seller_sheet->id) && !is_null($seller_sheet->reference))
                                <td><a href="{{route("seller_sheets.show",$seller_sheet->id)}}">{{$seller_sheet->reference}}</a></td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($seller_sheet->seller_id) && !is_null($seller_sheet->seller) && !is_null($seller_sheet->seller->name))
                                <td>{{$seller_sheet->seller->name}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($seller_sheet->unicorn_id) && !is_null($seller_sheet->unicorn) && !is_null($seller_sheet->unicorn->name))
                                <td>{{$seller_sheet->unicorn->name}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($seller_sheet->price))
                                <td>{{$seller_sheet->price}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($seller_sheet->sell_place))
                                <td>{{$seller_sheet->sell_place}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($seller_sheet->bought))
                                <td>{{$seller_sheet->bought ? __('payments.bought') : __('payments.not_bought')}}</td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('seller_sheets.seller_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('seller_sheets.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('seller_sheets.store')}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="reference">{{__('seller_sheets.reference')}}</label>
                            <input type="text" name="reference" id="reference" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="seller_id">{{__('seller_sheets.seller')}}</label>
                            <select name="seller_id" id="seller_id" class="form-control">
                                <option value="0"></option>
                                @foreach($sellers as $seller)
                                    <option value="{{$seller->id}}">{{$seller->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="unicorn_id">{{__('seller_sheets.unicorn')}}</label>
                            <select name="unicorn_id" id="unicorn_id" class="form-control">
                                <option value="0"></option>
                                @foreach($unicorns as $unicorn)
                                    <option value="{{$unicorn->id}}">{{$unicorn->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="price">{{__('seller_sheets.price')}}</label>
                            <input type="number" name="price" id="price" step="0.01" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="sell_place">{{__('seller_sheets.sell_place')}}</label>
                            <input type="text" name="sell_place" id="sell_place" class="form-control">
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

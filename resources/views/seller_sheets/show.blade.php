@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('seller_sheets.seller_sheets')}}</div>

                <div class="card-body">
                    <a href="{{ route('seller_sheets.index') }}" class="btn btn-secondary">{{__('forms.return_to_list')}}</a>
                    <a href="{{ route('seller_sheets.edit',$seller_sheet->id) }}" class="btn btn-info">{{__('forms.edit').__('seller_sheets.seller_sheet')}}</a>
                    <form action="{{ route('seller_sheets.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $seller_sheet->id }}">
                        <button class="btn btn-danger" type="submit">{{__('forms.delete').__('seller_sheets.seller_sheet')}}</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="reference" class="col-sm-2 col-form-label">{{__('seller_sheets.reference')}}</label>
                            @if(!is_null($seller_sheet->reference))
                            <div class="col-sm-10">
                                <input id="reference" readonly type="text" name="reference" class="form-control-plaintext" value="{{$seller_sheet->reference}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="seller" class="col-sm-2 col-form-label">{{__('seller_sheets.seller')}}</label>
                            @if(!is_null($seller_sheet->seller_id) && !is_null($seller_sheet->seller) && !is_null($seller_sheet->seller->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="seller" id="seller" class="form-control-plaintext" value="{{$seller_sheet->seller->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="unicorn" class="col-sm-2 col-form-label">{{__('seller_sheets.unicorn')}}</label>
                            @if(!is_null($seller_sheet->unicorn_id) && !is_null($seller_sheet->unicorn) && !is_null($seller_sheet->unicorn->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="unicorn" id="unicorn" class="form-control-plaintext" value="{{$seller_sheet->unicorn->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-sm-2 col-form-label">{{__('seller_sheets.price')}}</label>
                            @if(!is_null($seller_sheet->price))
                            <div class="col-sm-10">
                                <input type="text" readonly name="price" id="price" class="form-control-plaintext" value="{{$seller_sheet->price}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="sell_place" class="col-sm-2 col-form-label">{{__('seller_sheets.sell_place')}}</label>
                            @if(!is_null($seller_sheet->sell_place))
                            <div class="col-sm-10">
                                <input type="text" readonly name="sell_place" id="sell_place" class="form-control-plaintext" value="{{$seller_sheet->sell_place}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="bought" class="col-sm-2 col-form-label">{{__('payments.bought')}}</label>
                            @if(!is_null($seller_sheet->bought))
                            <div class="col-sm-10">
                                <input type="text" readonly name="bought" id="bought" class="form-control-plaintext" value="{{$seller_sheet->bought ? __('payments.bought') : __('payments.not_bought')}}">
                            </div>
                            @endif
                        </div>
                    </form>

                    @if(!is_null($seller_sheet->bought) && !$seller_sheet->bought)
                    <a href="{{ route('payments.buy',$seller_sheet->id) }}" class="btn btn-info">{{__('payments.buy').__('unicorns.unicorn')}}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

{{-- Stripe js  --}}
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ asset('js/stripe.js') }}">

</script>

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('seller_sheets.seller_sheet')}}</div>


                <div class="card-body">
                    <input type="hidden" id="public_key" value="{{ config('stripe.public_key') }}">
                    <form action="{{ route('payments.pay') }}" method="post" id="payment-form">
                        @csrf

                        <div class="form-row">
                            <label for="card-element">
                                {{__('payments.credit_card')}}
                            </label>
                            <div id="card-element" class="form-control">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display form errors -->
                            <div id="card-errors" role="alert"></div>
                        </div>
                        <br>
                        <input type="submit" class="btn btn-primary" value="{{ __('payments.buy') }} ({{ $seller_sheet->price}}€)">

                        <div class="sheet_informations">
                            <input type="hidden" name="id" value="{{ $seller_sheet->id }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

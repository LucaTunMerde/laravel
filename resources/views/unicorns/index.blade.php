@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('unicorns.unicorns')}}</div>

                <div class="card-body">
                    <a href="{{ route('unicorns.create') }}" class="btn btn-secondary">{{__('forms.add').__('unicorns.unicorn')}}</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{__('unicorns.name')}}</th>
                                <th scope="col">{{__('unicorns.tourniquet')}}</th>
                                <th scope="col">{{__('unicorns.birth_date')}}</th>
                                <th scope="col">{{__('unicorns.birth_place')}}</th>
                                <th scope="col">{{__('unicorns.mane_color')}}</th>
                                <th scope="col">{{__('unicorns.sex')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($unicorns as $unicorn)
                            <tr>
                                @if(!is_null($unicorn->id) && !is_null($unicorn->name))
                                <td><a href="{{route("unicorns.show",$unicorn->id)}}">{{$unicorn->name}}</a></td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($unicorn->tourniquet))
                                <td>{{$unicorn->tourniquet}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($unicorn->birth_date))
                                <td>{{$unicorn->birth_date}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($unicorn->birth_place))
                                <td>{{$unicorn->birth_place}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($unicorn->mane_color))
                                <td>{{$unicorn->mane_color}}</td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($unicorn->sex))
                                <td>{{!$unicorn->sex ? __('unicorns.male'):__('unicorns.female')}}</td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

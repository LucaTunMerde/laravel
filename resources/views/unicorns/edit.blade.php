@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('unicorns.unicorns')}}</div>

                <div class="card-body">
                    <a href="{{ route('unicorns.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('unicorns.update',$unicorn->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">{{__('unicorns.name')}}</label>
                            @if(!is_null($unicorn->name))
                                <input id="name" type="text" name="name" class="form-control" value="{{$unicorn->name}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="tourniquet">{{__('unicorns.tourniquet')}}</label>
                            @if(!is_null($unicorn->tourniquet))
                            <input id="tourniquet" type="text" name="tourniquet" class="form-control" value="{{$unicorn->tourniquet}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="birth_date">{{__('unicorns.birth_date')}}</label>
                            @if(!is_null($unicorn->birth_date))
                            <input id="birth_date" type="date" name="birth_date" class="form-control" value="{{$unicorn->birth_date}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="birth_place">{{__('unicorns.birth_place')}}</label>
                            @if(!is_null($unicorn->birth_place))
                            <input id="birth_place" type="text" name="birth_place" class="form-control" value="{{$unicorn->birth_place}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="mane_color">{{__('unicorns.mane_color')}}</label>
                            @if(!is_null($unicorn->mane_color))
                            <input id="mane_color" type="text" name="mane_color" class="form-control" value="{{$unicorn->mane_color}}">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="sex">{{__('unicorns.sex')}}</label>
                            <select class="form-control" name="sex" id="sex">
                                <option value="0" {{$unicorn->sex == 0 ? 'selected':''}} >{{__('unicorns.male')}}</option>
                                <option value="1" {{$unicorn->sex == 1 ? 'selected':''}}>{{__('unicorns.female')}}</option>
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('unicorns.unicorns')}}</div>

                <div class="card-body">
                    <a href="{{ route('unicorns.index') }}" class="btn btn-secondary">{{__('forms.return_to_list')}}</a>
                    <a href="{{ route('unicorns.edit',$unicorn->id) }}" class="btn btn-info">{{__('forms.edit').__('unicorns.unicorn')}}</a>
                    <form action="{{ route('unicorns.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $unicorn->id }}">
                        <button class="btn btn-danger" type="submit">{{__('forms.delete').__('unicorns.unicorn')}}</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">{{__('unicorns.name')}}</label>
                            @if(!is_null($unicorn->name))
                            <div class="col-sm-10">
                                <input id="name" readonly type="text" name="name" class="form-control-plaintext" value="{{$unicorn->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="tourniquet" class="col-sm-2 col-form-label">{{__('unicorns.tourniquet')}}</label>
                            @if(!is_null($unicorn->tourniquet))
                            <div class="col-sm-10">
                                <input type="text" readonly name="tourniquet" id="tourniquet" class="form-control-plaintext" value="{{$unicorn->tourniquet}}">
                            </div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="birth_date" class="col-sm-2 col-form-label">{{__('unicorns.birth_date')}}</label>
                            @if(!is_null($unicorn->birth_date))
                            <div class="col-sm-10">
                                <input type="text" readonly name="birth_date" id="birth_date" class="form-control-plaintext" value="{{$unicorn->birth_date}}">
                            </div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="birth_place" class="col-sm-2 col-form-label">{{__('unicorns.birth_place')}}</label>
                            @if(!is_null($unicorn->birth_place))
                            <div class="col-sm-10">
                                <input type="text" readonly name="birth_place" id="birth_place" class="form-control-plaintext" value="{{$unicorn->birth_place}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="mane_color" class="col-sm-2 col-form-label">{{__('unicorns.mane_color')}}</label>
                            @if(!is_null($unicorn->mane_color))
                            <div class="col-sm-10">
                                <input type="text" readonly name="mane_color" id="mane_color" class="form-control-plaintext" value="{{$unicorn->mane_color}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="sex" class="col-sm-2 col-form-label">{{__('unicorns.sex')}}</label>
                            @if(!is_null($unicorn->sex))
                            <div class="col-sm-10">
                                <input type="text" readonly name="sex" id="sex" class="form-control-plaintext" value="{{!$unicorn->sex?__('unicorns.male'):__('unicorns.female')}}">
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('unicorns.unicorns')}}</div>

                <div class="card-body">
                    <a href="{{ route('unicorns.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('unicorns.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{__('unicorns.name')}}</label>
                            <input id="name" type="text" name="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="tourniquet">{{__('unicorns.tourniquet')}}</label>
                            <input id="tourniquet" type="text" name="tourniquet" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="birth_date">{{__('unicorns.birth_date')}}</label>
                            <input id="birth_date" type="date" name="birth_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="birth_place">{{__('unicorns.birth_place')}}</label>
                            <input id="birth_place" type="text" name="birth_place" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="mane_color">{{__('unicorns.mane_color')}}</label>
                            <input id="mane_color" type="text" name="mane_color" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="sex">{{__('unicorns.sex')}}</label>
                            <select class="form-control" name="sex" id="sex">
                                <option value="0">{{__('unicorns.male')}}</option>
                                <option value="1">{{__('unicorns.female')}}</option>
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

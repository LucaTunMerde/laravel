@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id="accordion">
                @if(count($results["users"]))
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                {{__('users.users')}}
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">

                        @foreach($results['users'] as $user)
                        <div class="card-body">
                            <a href="{{route('users.show',$user->id)}}">{{ $user->name }}</a>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                @if(count($results["unicorns"]))
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                {{__('unicorns.unicorns')}}
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        @foreach($results['unicorns'] as $unicorn)
                        <div class="card-body">
                            <a href="{{route('unicorns.show',$unicorn->id)}}">{{ $unicorn->name }}</a>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                @if(count($results["seller_sheets"]))
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                {{__('seller_sheets.seller_sheets')}}
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        @foreach($results['seller_sheets'] as $seller_sheet)
                        <div class="card-body">
                            <a href="{{route('seller_sheets.show',$seller_sheet->id)}}">{{ $seller_sheet->reference }}</a>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                @if(count($results["breeder_sheets"]))
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                {{__('breeder_sheets.breeder_sheets')}}
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        @foreach($results['breeder_sheets'] as $breeder_sheet)
                        <div class="card-body">
                            <a href="{{route('breeder_sheets.show',$breeder_sheet->id)}}">{{ $breeder_sheet->reference }}</a>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('roles.roles')}}</div>

                <div class="card-body">
                    <a href="{{ route('roles.create') }}" class="btn btn-secondary">{{__('forms.add').__('roles.role')}}</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">{{__('roles.name')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                @if(!is_null($role->id) && !is_null($role->name))
                                <td><a href="{{route("roles.show",$role->id)}}">{{$role->name}}</a></td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

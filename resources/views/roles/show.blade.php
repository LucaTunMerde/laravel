@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('roles.roles')}}</div>

                <div class="card-body">
                    <a href="{{ route('roles.index') }}" class="btn btn-secondary">{{__('forms.return_to_list')}}</a>
                    <a href="{{ route('roles.edit',$role->id) }}" class="btn btn-info">{{__('forms.edit').__('roles.role')}}</a>
                    <form action="{{ route('roles.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $role->id }}">
                        <button class="btn btn-danger" type="submit">{{__('forms.delete').__('roles.role')}}</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">{{__('roles.name')}}</label>
                            @if(!is_null($role->name))
                            <div class="col-sm-10">
                                <input id="name" readonly type="text" name="name" class="form-control-plaintext" value="{{$role->name}}">
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

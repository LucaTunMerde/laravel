@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('roles.roles')}}</div>

                <div class="card-body">
                    <a href="{{ route('roles.index') }}" class="btn btn-danger">{{__('forms.return_to_list')}}</a>

                    <br/>
                    <br/>

                    <form action="{{route('roles.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{__('roles.name')}}</label>
                            <input id="name" type="text" name="name" class="form-control">
                        </div>

                        <button class="btn btn-success" type="submit" name="button">{{__('forms.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

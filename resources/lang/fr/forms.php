<?php

return [

    'return_to_list' => 'Retour à la liste',
    'list' => 'Liste des ',
    'submit' => 'Enregister',
    'show' => 'Voir une ',
    'add' => 'Ajouter une ',
    'edit' => 'Modifier une ',
    'delete' => 'Supprimer une '

];

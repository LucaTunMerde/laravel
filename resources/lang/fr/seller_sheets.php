<?php

return [

    'seller_sheets' => 'Fiches Vente',
    'seller_sheet' => 'Fiche Vente',
    'seller' => 'Vendeur',
    'unicorn' => 'Licorne',
    'reference' => 'Réference',
    'price' => 'Prix',
    'sell_place' => 'Lieu de vente'

];

<?php

return [

    'users' => 'Utilisteurs',
    'user' => 'Utilisateur',
    'name' => 'Nom',
    'email' => 'Email',
    'role' => 'Rôle'

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Votre mot de passe à été réinitialisé!',
    'sent' => 'Un email vous à été envoyé avec un lien de réinitialisation de mot de passe!',
    'throttled' => 'Merci de patienter avant de réessayer.',
    'token' => 'Ce lien de réinitialisation de mot de passe est invalide.',
    'user' => "Aucun utlisateur avec cet email n'existe.",

];

<?php

return [

    'breeder_sheets' => 'Fiches Éleveur',
    'breeder_sheet' => 'Fiche Éleveur',
    'reference' => 'Référence',
    'breeder' => 'Éleveur',
    'unicorn' => 'Licorne',
    'reproduction_date1' => 'Première date de reproduction',
    'reproduction_date2' => 'Seconde date de reproduction',
    'pregnant' => 'Enceinte',
    'notPregnant' => 'Pas enceinte',
    'nb_childs' => 'Nombre de poulains'

];

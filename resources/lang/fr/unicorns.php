<?php

return [

    'unicorns' => 'Licornes',
    'unicorn' => 'Licorne',
    'name' => 'Nom',
    'tourniquet' => 'Garrot',
    'birth_date' => 'Date de naissance',
    'birth_place' => 'Lieu de naissance',
    'mane_color' => 'Couleur de la crinière',
    'sex' => 'Sexe',
    'male' => 'Étalon',
    'female' => 'Jument'
];

<?php

return [

    'buy' => 'Acheter ',
    'credit_card' => 'Carte de crédit ou de débit',
    'error' => 'Cette licorne ne peux pas être achetée, veuillez contacter un administateur pour en savoir plus.',
    'success_unicorn' => 'Félicitations, vous venez d\'acheter cette licorne. <br>
                            Vous allez être mis en relation avec le propriétaire dans les prochains jours.',
    'bought' => 'Acheté',
    'not_bought' => 'Disponible'
];

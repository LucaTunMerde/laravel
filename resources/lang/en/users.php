<?php

return [

    'users' => 'Users',
    'user' => 'User',
    'name' => 'Name',
    'email' => 'Email',
    'role' => 'Role'

];

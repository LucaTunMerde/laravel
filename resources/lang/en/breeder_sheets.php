<?php

return [

    'breeder_sheets' => 'Breeder sheets',
    'breeder_sheet' => 'Breeder sheet',
    'reference' => 'Reference',
    'breeder' => 'Breeder',
    'unicorn' => 'Unicorn',
    'reproduction_date1' => 'First reproduction date',
    'reproduction_date2' => 'Second reproduction date',
    'pregnant' => 'Pregnant',
    'notPregnant' => 'Not pregnant',
    'nb_childs' => 'Number of childs'

];

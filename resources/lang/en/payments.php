<?php

return [

    'buy' => 'Buy ',
    'credit_card' => 'Credit or debit card',
    'error' => 'This unicorn can\'t be bought,<br> please contact an administator to see why.',
    'success_unicorn' => 'Congratulations, you just bought this unicorn. <br>
                            You will be put in contact with the owner in the next few days.',
    'bought' => 'Bought',
    'not_bought' => 'Available'

];

<?php

return [

    'unicorns' => 'Unicorns',
    'unicorn' => 'Unicorn',
    'name' => 'Name',
    'tourniquet' => 'Tourniquet',
    'birth_date' => 'Date of birth',
    'birth_place' => 'Place of birth',
    'mane_color' => 'Color of the mane',
    'sex' => 'Sex',
    'male' => 'Stallion',
    'female' => 'Mare'

];

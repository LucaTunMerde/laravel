<?php

return [

    'app_name' => 'Adopt a Unicorn',
    'number of' => 'Number of ',
    'search' => 'Search',
    'yes' => 'Yes',
    'no' => 'No'
];

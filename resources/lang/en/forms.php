<?php

return [

    'return_to_list' => 'Return to list',
    'list' => 'List of ',
    'submit' => 'Submit',
    'show' => 'Show a ',
    'add' => 'Add a ',
    'edit' => 'Edit a ',
    'delete' => 'Delete a '

];

<?php

return [

    'seller_sheets' => 'Seller sheets',
    'seller_sheet' => 'Seller sheet',
    'seller' => 'Seller',
    'unicorn' => 'Unicorn',
    'reference' => 'Reference',
    'price' => 'Price',
    'sell_place' => 'Sell place'

];

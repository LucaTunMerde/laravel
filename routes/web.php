<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Auth\Middleware\Authenticate;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/lang', 'HomeController@postChangeLanguage')->name('change_language');
Route::post('/home/search', 'HomeController@search')->name('search');


/* Users routes */
Route::get('/users', 'UserController@index')->name('users.index');

Route::get('/users/create', 'UserController@create')->name('users.create');
Route::post('/users', 'UserController@store')->name('users.store');

Route::get('/users/{id}/show', 'UserController@show')->name('users.show');

Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
Route::put('/users/{id}', 'UserController@update')->name('users.update');

Route::delete('/users', 'UserController@destroy')->name('users.destroy');



/* Roles routes */
Route::get('/roles', 'RoleController@index')->name('roles.index');

Route::get('/roles/create', 'RoleController@create')->name('roles.create');
Route::post('/roles', 'RoleController@store')->name('roles.store');

Route::get('/roles/{id}/show', 'RoleController@show')->name('roles.show');

Route::get('/roles/{id}/edit', 'RoleController@edit')->name('roles.edit');
Route::put('/roles/{id}', 'RoleController@update')->name('roles.update');

Route::delete('/roles', 'RoleController@destroy')->name('roles.destroy');



/* Unicorns routes */
Route::get('/unicorns', 'UnicornController@index')->name('unicorns.index');

Route::get('/unicorns/create', 'UnicornController@create')->name('unicorns.create');
Route::post('/unicorns', 'UnicornController@store')->name('unicorns.store');

Route::get('/unicorns/{id}/show', 'UnicornController@show')->name('unicorns.show');

Route::get('/unicorns/{id}/edit', 'UnicornController@edit')->name('unicorns.edit');
Route::put('/unicorns/{id}', 'UnicornController@update')->name('unicorns.update');

Route::delete('/unicorns', 'UnicornController@destroy')->name('unicorns.destroy');


/* Seller sheets routes */
Route::get('/seller_sheets', 'Seller_sheetController@index')->name('seller_sheets.index');

Route::get('/seller_sheets/create', 'Seller_sheetController@create')->name('seller_sheets.create')->middleware('auth');
Route::post('/seller_sheets', 'Seller_sheetController@store')->name('seller_sheets.store')->middleware('auth');

Route::get('/seller_sheets/{id}/show', 'Seller_sheetController@show')->name('seller_sheets.show');

Route::get('/seller_sheets/{id}/edit', 'Seller_sheetController@edit')->name('seller_sheets.edit')->middleware('auth');
Route::put('/seller_sheets/{id}', 'Seller_sheetController@update')->name('seller_sheets.update')->middleware('auth');

Route::delete('/seller_sheets', 'Seller_sheetController@destroy')->name('seller_sheets.destroy')->middleware('auth');


/* Breeder sheets routes */
Route::get('/breeder_sheets', 'Breeder_sheetController@index')->name('breeder_sheets.index');

Route::get('/breeder_sheets/create', 'Breeder_sheetController@create')->name('breeder_sheets.create');
Route::post('/breeder_sheets', 'Breeder_sheetController@store')->name('breeder_sheets.store');

Route::get('/breeder_sheets/{id}/show', 'Breeder_sheetController@show')->name('breeder_sheets.show');

Route::get('/breeder_sheets/{id}/edit', 'Breeder_sheetController@edit')->name('breeder_sheets.edit');
Route::put('/breeder_sheets/{id}', 'Breeder_sheetController@update')->name('breeder_sheets.update');

Route::delete('/breeder_sheets', 'Breeder_sheetController@destroy')->name('breeder_sheets.destroy');


/* Payment with stripe routes */
Route::get('/seller_sheets/{id}/buy', 'PaymentController@buy')->name('payments.buy');

Route::post('/payment', 'PaymentController@paymentProcess')->name('payments.pay');
